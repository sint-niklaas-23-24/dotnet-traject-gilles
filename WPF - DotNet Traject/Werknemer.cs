﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WPF___DotNet_Traject
{
    internal class Werknemer : Persoon
    {
        private Guid _werknemersnummer;
        private double _loon;

        public Werknemer() : base()
        { 
            _werknemersnummer = Guid.NewGuid();
        }
        public Werknemer(string voornaam, string achternaam, BitmapImage avatar, double loon) : base(voornaam, achternaam, avatar)
        {
            _werknemersnummer = Guid.NewGuid();
            Loon = loon;
        }

        public Guid Werknemersnummer
        {
            get { return _werknemersnummer; }
        }
        public double Loon
        {
            get { return _loon; }
            set { _loon = value; }
        }

        public override string Details()
        {
            return base.Details() + Werknemersnummer + " " + Loon + "€";
        }
    }
}
