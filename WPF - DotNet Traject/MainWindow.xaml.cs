﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;

namespace WPF___DotNet_Traject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Cursist cursist1 = new Cursist("Gilles", "Fabry", new BitmapImage(new Uri(@"Images\Gilles.JPG", UriKind.Relative)));
            Cursist cursist2 = new Cursist("Robin", "Boone", new BitmapImage(new Uri(@"Images\Robin.JPG", UriKind.Relative)));
            Cursist cursist3 = new Cursist("Bernd", "Herssens", new BitmapImage(new Uri(@"\Images\Bernd.JPG", UriKind.Relative)));
            lijstCursisten.Add(cursist1);
            lijstCursisten.Add(cursist2);
            lijstCursisten.Add(cursist3);
            LijstEnTXTRefreshen();
        }
        Cursist cursist;
        List<Cursist> lijstCursisten = new List<Cursist>();
        string filePad;
        string pad = Environment.CurrentDirectory;
        BitmapImage eenAvatar;

        private void btnAfbeeldingZoeken_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog verkenner = new OpenFileDialog()
                {
                    InitialDirectory = pad,
                };
                if (verkenner.ShowDialog() == true)
                {
                    filePad = verkenner.FileName;
                    eenAvatar = new BitmapImage(new Uri(filePad));
                }
                else
                {
                    MessageBox.Show("Afbeeldingsselectie geannuleerd!", "Geannuleerd", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void btnOpslaan_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (gbCursisten.SelectedIndex == -1)
                {
                    //cursist aanmaken
                    cursist = new Cursist(txtVoornaam.Text, txtAchternaam.Text, eenAvatar);
                    lijstCursisten.Add(cursist);
                    MessageBox.Show($"Welkom {txtVoornaam.Text} {txtAchternaam.Text}!", "DotNet Traject", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else if (gbCursisten.SelectedIndex != -1)
                {
                    //cursist wijzigen
                    lijstCursisten[gbCursisten.SelectedIndex].Voornaam = txtVoornaam.Text;
                    lijstCursisten[gbCursisten.SelectedIndex].Achternaam = txtAchternaam.Text;
                    lijstCursisten[gbCursisten.SelectedIndex].Avatar = eenAvatar;
                }
                lijstCursisten = lijstCursisten.OrderBy(x => x.Voornaam).ToList();
                LijstEnTXTRefreshen();
            }
            catch (Exception ex)
            {

            }
        }
        private void btnVerwijderen_Click(object sender, RoutedEventArgs e)
        {
            if (gbCursisten.SelectedIndex != -1)
            {
                lijstCursisten.RemoveAt(gbCursisten.SelectedIndex);
                LijstEnTXTRefreshen();
            } else
            {
                MessageBox.Show("U moet eerst een cursist selecteren alvorens te kunnen verwijderen!", "Geen selectie", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void gbCursisten_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (gbCursisten.SelectedIndex != -1)
            {
                txtVoornaam.Text = lijstCursisten[gbCursisten.SelectedIndex].Voornaam;
                txtAchternaam.Text = lijstCursisten[gbCursisten.SelectedIndex].Achternaam;
                eenAvatar = lijstCursisten[gbCursisten.SelectedIndex].Avatar;
            }
        }
        public void LijstEnTXTRefreshen()
        {
            gbCursisten.ItemsSource = null;
            gbCursisten.ItemsSource = lijstCursisten;
            lbIngeschrevenCursisten.ItemsSource = null;
            lbIngeschrevenCursisten.ItemsSource = lijstCursisten;
            txtVoornaam.Text = null;
            txtAchternaam.Text = null;
        }
    }
}
