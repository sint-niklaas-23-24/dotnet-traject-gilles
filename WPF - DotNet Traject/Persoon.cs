﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WPF___DotNet_Traject
{
    internal class Persoon
    {
        private string _voornaam;
        private string _achternaam;
        private BitmapImage _avatar;

        public Persoon() { }
        public Persoon(string voornaam, string achternaam, BitmapImage avatar)
        {
            Voornaam = voornaam;
            Achternaam = achternaam;
            Avatar = avatar;
        }
        public Persoon(string voornaam, string achternaam)
        {
            Voornaam= voornaam;
            Achternaam= achternaam;
        }

        public string Voornaam
        {
            get { return _voornaam; }
            set 
            { 
                if (!String.IsNullOrEmpty(value)) 
                { 
                    _voornaam = value; 
                } else
                {
                    throw new FormatException("U heeft geen voornaam en/of achternaam ingegeven.");
                }
            }
        }
        public string Achternaam
        {
            get { return _achternaam; }
            set 
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _achternaam = value;
                } else
                {
                    throw new FormatException("U heeft geen voornaam en/of achternaam ingegeven.");
                }
            }
        }
        public BitmapImage Avatar
        {
            get { return _avatar; }
            set 
            {
                if (value != null) 
                {
                    _avatar = value;
                } else
                {
                    throw new Exception("U heeft geen afbeelding geselecteerd.");
                }
            }
        }
        public virtual string Details()
        {
            return Avatar + " " + Voornaam + " " + Achternaam;
        }
        public override bool Equals(object? obj)
        {
            bool resultaat = false;
            //3 dingen doen:
            //1. Controleren of mijn object niet gelijk is aan null. COPY PASTE
            if (obj != null)
            {
                //2. Controleren of het datatype gelijk is aan elkaar. COPY PASTE
                if (GetType() == obj.GetType())
                {
                    //3. Eigen validatie
                    Persoon persoon = (Persoon)obj;
                    if (this.Voornaam == persoon.Voornaam && this.Achternaam == persoon.Achternaam)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }
    }
}
