﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WPF___DotNet_Traject
{
    internal class Cursist : Persoon
    {
        private Guid _cursistennummer;

        public Cursist() : base() 
        { 
            _cursistennummer = Guid.NewGuid();
        }
        public Cursist(string voornaam, string achternaam, BitmapImage afbeelding) : base(voornaam, achternaam, afbeelding)
        {
            _cursistennummer = Guid.NewGuid();
        }
        public Cursist(string voornaam, string achternaam) : base(voornaam, achternaam)
        {
            _cursistennummer = Guid.NewGuid();
        }

        public Guid Cursistennummer
        {
            get { return _cursistennummer; }
        }

        public override string Details()
        {
            return base.Details() + Cursistennummer;
        }
    }
}
